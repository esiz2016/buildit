package com.buildit.hiring.application.dto;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPlantInventoryRepDTO is a Querydsl query type for PlantInventoryRepDTO
 */
@Generated("com.mysema.query.codegen.EmbeddableSerializer")
public class QPlantInventoryRepDTO extends BeanPath<PlantInventoryRepDTO> {

    private static final long serialVersionUID = 1535741211L;

    public static final QPlantInventoryRepDTO plantInventoryRepDTO = new QPlantInventoryRepDTO("plantInventoryRepDTO");

    public final NumberPath<Long> _id = createNumber("_id", Long.class);

    public final StringPath description = createString("description");

    public final StringPath name = createString("name");

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public QPlantInventoryRepDTO(String variable) {
        super(PlantInventoryRepDTO.class, forVariable(variable));
    }

    public QPlantInventoryRepDTO(Path<? extends PlantInventoryRepDTO> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPlantInventoryRepDTO(PathMetadata<?> metadata) {
        super(PlantInventoryRepDTO.class, metadata);
    }

}

