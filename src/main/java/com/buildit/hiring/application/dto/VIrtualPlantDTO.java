package com.buildit.hiring.application.dto;

import lombok.Data;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Service;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

/**
 * Created by omitobisam on 25.05.16.
 */
@Data
@Service
public class VIrtualPlantDTO extends ResourceSupport {

    Long _id;
    String name;
    String description;
    BigDecimal price;
    BusinessPeriodDTO rentalPeriod;
}
