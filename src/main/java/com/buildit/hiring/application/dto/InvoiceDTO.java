package com.buildit.hiring.application.dto;

import com.buildit.hiring.application.domain.model.InvoiceStatus;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by omitobisam on 5.05.16.
 */
@Data
public class InvoiceDTO  extends ResourceSupport{
    Long _id;
    LocalDate date;

    String po;

    @Column(precision = 8, scale = 2)
    BigDecimal total;


    @Enumerated(EnumType.STRING)
    InvoiceStatus invoiceStatus;

    Boolean isAccepted;
}
