
package com.buildit.hiring.application.dto;

import lombok.*;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Embeddable;
import java.time.LocalDate;

/**
 * Created by omitobisam on 7.04.16.
 */
@Embeddable
@Data
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@AllArgsConstructor(staticName = "of")
public class BusinessPeriodDTO extends ResourceSupport {
    LocalDate startDate;
    LocalDate endDate;
}
