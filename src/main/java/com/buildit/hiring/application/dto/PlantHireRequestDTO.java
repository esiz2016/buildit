package com.buildit.hiring.application.dto;


import com.buildit.common.infrastructure.domain.BusinessPeriod;
import com.buildit.hiring.application.domain.model.PHRStatus;
import lombok.Data;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Service
public class PlantHireRequestDTO extends ResourceSupport {

    Long _id;

    String plantName;
    String plant; //plant is the URL String

    /*@Embedded
    PurchaseOrderDTO purchaseOrder;*/

    @Embedded
    BusinessPeriodDTO rentalPeriod;

    @Column(precision = 8, scale = 2)
    BigDecimal cost;

    @Enumerated (EnumType.STRING)
    PHRStatus status;

    String comment;

    String siteId;
    String email;
}
