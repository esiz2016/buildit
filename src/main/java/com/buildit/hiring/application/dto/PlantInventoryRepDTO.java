package com.buildit.hiring.application.dto;

import lombok.*;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Service;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.net.URL;

/**
 * Created by omitobisam on 30.03.16.
 */
@Data
@Embeddable
@Service
public class PlantInventoryRepDTO extends ResourceSupport{
    Long _id;
    String name;
    String description;
    BigDecimal price;
}
