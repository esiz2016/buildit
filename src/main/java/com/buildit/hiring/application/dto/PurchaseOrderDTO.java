package com.buildit.hiring.application.dto;

import com.buildit.common.infrastructure.domain.BusinessPeriod;
import com.buildit.hiring.application.domain.model.POStatus;
import lombok.Data;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;

/**
 * Created by omitobisam on 30.03.16.
 */
@Data
public class PurchaseOrderDTO extends ResourceSupport{
    Long _id;

    String po;

    @Enumerated(EnumType.STRING)
    POStatus status;
//
//    @Embedded
//    BusinessPeriodDTO rentalPeriod;
//
//    @Enumerated(EnumType.STRING)
//    POStatus status;



}
