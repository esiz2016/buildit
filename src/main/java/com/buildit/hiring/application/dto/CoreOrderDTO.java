package com.buildit.hiring.application.dto;

import com.buildit.hiring.application.domain.model.POStatus;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Service;

import javax.persistence.Column;
import javax.persistence.Embedded;
import java.math.BigDecimal;

/**
 * Created by omitobisam on 19.05.16.
 */
@Service
@Data
public class CoreOrderDTO extends ResourceSupport {
    Long _id;
    @Embedded
    PlantInventoryRepDTO plant;

    @Embedded
    BusinessPeriodDTO rentalPeriod;
    @Column(precision = 8, scale = 2)
    BigDecimal total;
    POStatus status;

    String email;
}
