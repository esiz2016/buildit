package com.buildit.hiring.application.Assembler;

import com.buildit.hiring.application.domain.model.PlantHireRequest;
import com.buildit.hiring.application.dto.BusinessPeriodDTO;
import com.buildit.hiring.application.dto.PlantHireRequestDTO;
import com.buildit.hiring.application.dto.PlantInventoryRepDTO;
import com.buildit.integration.controller.PlantHireRequestRestController;
import com.buildit.integration.service.ProcurementService;
import com.buildit.integration.service.RentalService;
import com.buildit.integration.service.idgenerator.ProcurementIdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by omitobisam on 30.03.16.
 */
@Service
public class PHRAssembler   extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestDTO> {
        @Autowired
        RentalService rentalService;

        @Autowired
        ProcurementService procurementService;

        @Autowired
        ProcurementIdentifierGenerator proIDGen;

        public PHRAssembler() {
            super(PlantHireRequestRestController.class, PlantHireRequestDTO.class);
        }

        @Override
        public PlantHireRequestDTO toResource(PlantHireRequest plantHireRequest) {
            PlantHireRequestDTO phrdto = createResourceWithId(plantHireRequest.getId().getId(), plantHireRequest);
            phrdto.set_id(plantHireRequest.getId().getId());
            phrdto.setPlantName(plantHireRequest.getPlantName());
            phrdto.setCost(plantHireRequest.getCost());
            phrdto.setPlant(plantHireRequest.getPlant());
            phrdto.setRentalPeriod(BusinessPeriodDTO.of(plantHireRequest.getRentalPeriod().getStartDate(), plantHireRequest.getRentalPeriod().getEndDate()));
            phrdto.setComment(plantHireRequest.getComment());
            phrdto.setStatus(plantHireRequest.getStatus());
            phrdto.setSiteId(plantHireRequest.getSiteId());
            phrdto.setEmail(plantHireRequest.getContact_email());
            return phrdto;
        }

}
