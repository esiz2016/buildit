package com.buildit.hiring.application.Assembler;

import com.buildit.hiring.application.Assembler.InvoiceGateway;
import com.buildit.hiring.application.Assembler.InvoiceProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.mail.Mail;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

/**
 * Created by omitobisam on 3.05.16.
 */
@Service
@IntegrationComponentScan
@Configuration
public class InvoiceIntegrationFlow {

	@Autowired
	InvoiceGateway gateWays;

	@Autowired
	InvoiceProcessor invoiceProcessor;

//    @Value("${gmail.username")
    String gmailUsername = "rentit555";
//    @Value("${gmail.password")
    String gmailPassword = "123qaz123";

    //recieve Invoice flow
	@Bean
    public IntegrationFlow receiveInvoiceFlow(){

//            return IntegrationFlows
//                    .from(Mail.imapIdleAdapter(String.format("imaps://%s:%s@imap.gmail.com/INBOX", gmailUsername, gmailPassword))
//                            .selectorExpression("subject matches '.*invoice.*'"))
//                    .transform("@invoiceProcessor.extractInvoice(payload)")
//                    .route("#xpath(payload, '//total <= 800', 'string')", mapping -> mapping
//                            .subFlowMapping("true", sf -> sf
//                                    .handle("invoiceProcessor", "processInvoice"))
//                            .subFlowMapping("false", sf -> sf
//                                    .handle(System.out::println))
//                    )
//                    .get();

            return IntegrationFlows
                    .from(Mail.imapIdleAdapter(String.format("imaps://%s:%s@imap.gmail.com/INBOX", gmailUsername, gmailPassword))
                            .selectorExpression("subject matches '.*Invoice.*'")
                    )
                    .transform("@invoiceProcessor.extractInvoice(payload)")
                    //.handle(System.out::println)
                    //Using XPATH
                    .route("#xpath(payload, '//total <= 1200', 'string')", mapping -> mapping
                    .subFlowMapping("true", subflow -> subflow.handle("invoiceProcessor", "processInvoice"))
                    .subFlowMapping("false", subflow -> subflow.handle(msg -> System.out.println("it was false"))))

                    /*.<String,String>transform(m -> sirenToHal(m))*/
                    .get();
	}


	public void setInvoice(){

	}
}
