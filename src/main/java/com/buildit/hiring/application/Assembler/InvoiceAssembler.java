package com.buildit.hiring.application.Assembler;

import com.buildit.hiring.application.domain.model.Invoice;
import com.buildit.hiring.application.domain.model.PurchaseOrder;
import com.buildit.hiring.application.dto.CoreOrderDTO;
import com.buildit.hiring.application.dto.InvoiceDTO;
import com.buildit.hiring.application.dto.PurchaseOrderDTO;
import com.buildit.integration.controller.InvoiceRestController;
import com.buildit.integration.service.ProcurementService;
import com.buildit.integration.service.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by omitobisam on 4.05.16.
 */
@Service
public class InvoiceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceDTO> {
    @Autowired
    ProcurementService procurementService;

    @Autowired
    RentalService rentalService;

    public InvoiceAssembler() {
        super(InvoiceRestController.class, InvoiceDTO.class);
    }

    @Override
    public InvoiceDTO toResource(Invoice invoice) {
        InvoiceDTO dto = createResourceWithId(invoice.getId().getId(), invoice);
        dto.set_id(invoice.getId().getId());
        dto.setPo(invoice.getPo());
        dto.setIsAccepted(invoice.getIsAccepted());
        dto.setDate(invoice.getDate());
        dto.setTotal(invoice.getTotal());
        dto.setInvoiceStatus(invoice.getInvoiceStatus());
        return dto;
    }

    public BigDecimal calculateInvoice(String poURl){
        CoreOrderDTO coreOrderDTO = rentalService.fetchPurchaseOrder(procurementService.getUrlID(poURl));
        BigDecimal price = coreOrderDTO.getPlant().getPrice();
        LocalDate startDate = coreOrderDTO.getRentalPeriod().getStartDate();
        LocalDate endDate = coreOrderDTO.getRentalPeriod().getEndDate();
        int diffDays = endDate.getDayOfYear() - startDate.getDayOfYear();


        BigDecimal total = price.multiply(BigDecimal.valueOf(Double.valueOf(diffDays)));

        return total;
    }

    public boolean checkPOMatch(String poURL){
        boolean status = false;
        List<PurchaseOrder> pos = procurementService.findAllPOS();
        for (PurchaseOrder po:pos
             ) {
            if (po.getPo().equals(poURL)){
                status = true;
            } else {}
        }
        return status;
    }

}
