package com.buildit.hiring.application.Assembler;

import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

/**
 * Created by omitobisam on 4.05.16.
 */
@Service
@IntegrationComponentScan
@Configuration
@MessagingGateway
public interface InvoiceGateway {
    @Gateway(requestChannel = "receiveInvoiceChannel")
    public void receiveInvoice(MimeMessage msg);
}
