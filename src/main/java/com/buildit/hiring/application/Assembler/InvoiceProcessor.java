package com.buildit.hiring.application.Assembler;

import com.buildit.hiring.application.domain.model.InvoiceStatus;
import com.buildit.hiring.application.dto.InvoiceDTO;
import com.buildit.integration.service.InvoiceService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.stereotype.Service;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by omitobisam on 3.05.16.
 */
@Configuration
@Service
@IntegrationComponentScan
public class InvoiceProcessor {


    @Autowired
    InvoiceAssembler invoiceAssembler;

    @Autowired
    InvoiceService invoiceService;

    public String extractUrl(String invoice){
        Pattern p = Pattern.compile("<purchaseOrderHRef>(.*?)</purchaseOrderHRef>");
        Matcher m = p.matcher(invoice);
        while(m.find())
        {
            return m.group(1);
        }
        return "";
    }

    public BigDecimal extractPrice(String invoice){
        Pattern p = Pattern.compile("<total>(.*?)</total>");
        Matcher m = p.matcher(invoice);
        while(m.find())
        {
            Double d = Double.parseDouble(m.group(1));
            return new BigDecimal(d);

        }
        return new BigDecimal(0.0);
    }




    public void processInvoice(String invoice) {
        BigDecimal price = extractPrice(invoice);
        String poUrl = extractUrl(invoice);

        boolean invMatchStat = invoiceAssembler.checkPOMatch(poUrl);
        System.out.println(invMatchStat + " PO");
        if(invMatchStat==true) {

            BigDecimal total = invoiceAssembler.calculateInvoice(poUrl);
            InvoiceDTO invoiceDTO = new InvoiceDTO();
            invoiceDTO.setPo(poUrl);
            invoiceDTO.setTotal(price);

            if (!total.equals(price)) {
                invoiceDTO.setIsAccepted(false);
            } else {
                invoiceDTO.setIsAccepted(true);
            }
            invoiceDTO.setDate(LocalDate.now());
            invoiceDTO.setInvoiceStatus(InvoiceStatus.UNPAID);

            invoiceService.createNewInvoice(invoiceDTO);
        } else {
            System.out.println("PO does not exist here!");
        }
//        invoiceDTO.setStatus(InvoiceStatus.ACCEPTED);
//        invoiceService.saveInvoice(invoiceDTO);
//        invoiceService.payInvoice(url,price);

//        System.out.println("PAYED");
//        System.out.println(invoice);
//
//
//
//        if(invoice.contains("Invoice")){
//            Invoice inv  = Invoice.of (
//                    new ProcurementIdentifierGenerator().hasNextInvoiceID(),
//                    LocalDate.now(),
//                    new BigDecimal(invoice.substring(invoice.indexOf("price"), invoice.lastIndexOf(0))));
//
//            inv.setInvoiceStatus(InvoiceStatus.UNPAID);
//            inv.isAccepted(true);
//        }
    }




    public String extractInvoice(MimeMessage msg) throws Exception{
        Multipart multipart = (Multipart) msg.getContent();
        for (int i = 0; i < multipart.getCount(); i++){
            BodyPart bodyPart = multipart.getBodyPart(i);
            if(bodyPart.getContentType().contains("xml") && bodyPart.getFileName().startsWith("invoice")){
                return IOUtils.toString(bodyPart.getInputStream(), "UTF-8").toString();
            }
        }
        throw new Exception("Oops no invoice found");
    }

}
