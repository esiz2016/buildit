package com.buildit.hiring.application.Assembler;

import com.buildit.common.infrastructure.domain.BusinessPeriod;
import com.buildit.hiring.application.domain.model.POStatus;
import com.buildit.hiring.application.domain.model.PurchaseOrder;
import com.buildit.hiring.application.dto.BusinessPeriodDTO;
import com.buildit.hiring.application.dto.PurchaseOrderDTO;
import com.buildit.integration.controller.PurchaseOrderRestController;
import com.buildit.integration.service.ProcurementService;
import com.buildit.integration.service.RentalService;
import com.buildit.integration.service.idgenerator.ProcurementIdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by omitobisam on 13.05.16.
 */
@Service
public class PurchaseOrderAssembler extends ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderDTO> {
    @Autowired
    ProcurementService procurementService;

    @Autowired
    ProcurementIdentifierGenerator pIDGen;

    @Autowired
    RentalService rentalService;



    public PurchaseOrderAssembler() {
        super(PurchaseOrderRestController.class, PurchaseOrderDTO.class);
    }

    @Override
    public PurchaseOrderDTO toResource(PurchaseOrder purchaseOrder) {
        PurchaseOrderDTO purchaseOrderDTO = createResourceWithId(purchaseOrder.getId().getHref(), purchaseOrder);
        purchaseOrderDTO.set_id(purchaseOrder.getId().getHref());
        purchaseOrderDTO.setPo(purchaseOrder.getPo());
        purchaseOrderDTO.setStatus(getPOStatus(purchaseOrder.getPo()));
//        purchaseOrderDTO.setPlant(purchaseOrder.getPlant());
//        purchaseOrderDTO.setRentalPeriod(BusinessPeriodDTO.of(purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getEndDate()));
//        purchaseOrderDTO.setStatus(purchaseOrder.getStatus());
        return purchaseOrderDTO;
    }

    public POStatus getPOStatus(String poURL){
        Long poID = procurementService.getUrlID(poURL);
        return rentalService.fetchPurchaseOrder(poID).getStatus();
    }

}
