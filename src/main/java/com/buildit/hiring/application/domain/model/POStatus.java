package com.buildit.hiring.application.domain.model;

/**
 * Created by omitobisam on 30.03.16.
 */
public enum POStatus {
    ACCEPTED, REJECTED, PENDING_CONFIRMATION, OPEN, APPROVED;
}
