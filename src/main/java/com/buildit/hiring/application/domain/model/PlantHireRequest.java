package com.buildit.hiring.application.domain.model;

import com.buildit.common.infrastructure.domain.BusinessPeriod;
import com.buildit.hiring.application.domain.IDs.PlantHireRequestID;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter @ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor(staticName = "of")
public class PlantHireRequest {
    @EmbeddedId
    PlantHireRequestID id;

    String plantName; //the name of the plant.
    String plant; //url is string

    @Embedded
    BusinessPeriod rentalPeriod;

    @Column(precision = 8, scale = 2)
    BigDecimal cost;

    String comment;

   @Enumerated (EnumType.STRING)
    PHRStatus status;

    String siteId;
    String contact_email;

//    public static PlantHireRequestEntry of(PlantHireRequestID id, HiredPlant){
//
//    }

//    @OneToOne
//    PurchaseOrder purchaseOrder;
    public void decidePHRStatus( PHRStatus phrstatus) {
        status = phrstatus;
    }
}
