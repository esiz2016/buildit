package com.buildit.hiring.application.domain.repository;

import com.buildit.hiring.application.domain.model.PlantHireRequest;
import com.buildit.hiring.application.domain.IDs.PlantHireRequestID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantHireRequestEntryRepository extends JpaRepository<PlantHireRequest, PlantHireRequestID>{

   /* @Query("UPDATE PlantHireRequestEntry p SET p.PHRStatus = ?1 WHERE p.id = ?2")
    PlantHireRequestEntry updateStatus(PHRStatus status, Long id);*/
}
