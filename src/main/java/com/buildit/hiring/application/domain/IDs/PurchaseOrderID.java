package com.buildit.hiring.application.domain.IDs;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.springframework.hateoas.Link;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by omitobisam on 30.03.16.
 */
@Embeddable
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class PurchaseOrderID implements Serializable {
    Long href;
}
