package com.buildit.hiring.application.domain.model;

import com.buildit.hiring.application.domain.IDs.InvoiceID;
import com.buildit.integration.service.idgenerator.ProcurementIdentifierGenerator;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by omitobisam on 3.05.16.
 */
@Entity
@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class Invoice {

    @EmbeddedId
    InvoiceID id;

    String po;

    LocalDate date;

    @Column(precision = 8, scale = 2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    InvoiceStatus invoiceStatus;

    Boolean isAccepted;



    public static  Invoice of(InvoiceID id, String po, LocalDate date, BigDecimal total) {
        Invoice invoice = new Invoice();
        invoice.id = id;
        invoice.po = po;
        invoice.date = date;
        invoice.total = total;
        return invoice;
    }

    public void setInvoiceStatus(InvoiceStatus invStatus){
        invoiceStatus = invStatus;
    }

    public void setAcceptStatus(boolean status){
        isAccepted = status;
    }
}
