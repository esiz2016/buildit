package com.buildit.hiring.application.domain.repository;

import com.buildit.hiring.application.domain.IDs.InvoiceID;
import com.buildit.hiring.application.domain.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by omitobisam on 5.05.16.
 */
@Repository
public interface InvoiceRepository extends JpaRepository <Invoice, InvoiceID>{
}
