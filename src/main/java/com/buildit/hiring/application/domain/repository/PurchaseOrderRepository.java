package com.buildit.hiring.application.domain.repository;


import com.buildit.hiring.application.domain.IDs.PurchaseOrderID;
import com.buildit.hiring.application.domain.model.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by omitobisam on 13.05.16.
 */
@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, PurchaseOrderID> {
}
