package com.buildit.hiring.application.domain.model;

import com.buildit.common.infrastructure.domain.BusinessPeriod;
import com.buildit.hiring.application.domain.IDs.PurchaseOrderID;
import lombok.*;

import javax.persistence.*;

/**
 * Created by omitobisam on 30.03.16.
 */
@Entity
@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor(staticName = "of")
public class PurchaseOrder {
    @EmbeddedId
    PurchaseOrderID id;
    String po;

/*    @Embedded
    BusinessPeriod rentalPeriod;

    @Enumerated(EnumType.STRING)
    POStatus status;*/

//    public void approveCoreOrder(){
//        this.status = POStatus.ACCEPTED;
//    }

}
