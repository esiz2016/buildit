package com.buildit.hiring.application.domain.model;

/**
 * Created by omitobisam on 3.05.16.
 */
public enum InvoiceStatus {
    PAID, UNPAID
}
