package com.buildit.hiring.application.domain.model;

import javax.persistence.Embeddable;

/**
 * Created by omitobisam on 30.03.16.
 */

public enum PHRStatus {
    APPROVED, REJECTED, CANCELED, PENDING_CONFIRMATION;
}
