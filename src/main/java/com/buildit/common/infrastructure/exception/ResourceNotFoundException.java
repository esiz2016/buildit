package com.buildit.common.infrastructure.exception;

/**
 * Created by omitobisam on 7.04.16.
 */
public class ResourceNotFoundException extends Exception {
        private static final long serialVersionUID = -7057332090589800040L;
}
