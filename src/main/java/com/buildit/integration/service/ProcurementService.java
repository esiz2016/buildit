package com.buildit.integration.service;

import com.buildit.common.infrastructure.domain.BusinessPeriod;
import com.buildit.hiring.application.Assembler.PurchaseOrderAssembler;
import com.buildit.hiring.application.domain.IDs.PlantHireRequestID;
import com.buildit.hiring.application.domain.IDs.PurchaseOrderID;
import com.buildit.hiring.application.domain.model.POStatus;
import com.buildit.hiring.application.domain.model.PurchaseOrder;
import com.buildit.hiring.application.domain.model.PHRStatus;
import com.buildit.hiring.application.domain.model.PlantHireRequest;
import com.buildit.hiring.application.domain.repository.PurchaseOrderRepository;
import com.buildit.hiring.application.domain.repository.PlantHireRequestEntryRepository;
import com.buildit.hiring.application.dto.PlantHireRequestDTO;
import com.buildit.hiring.application.dto.PurchaseOrderDTO;
import com.buildit.integration.service.idgenerator.ProcurementIdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by omitobisam on 31.03.16.
 */

@Service
public class ProcurementService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    PlantHireRequestEntryRepository phrRepo;

    @Autowired
    PurchaseOrderRepository poRepo;

    @Autowired
    PurchaseOrderRepository coRepo;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    ProcurementIdentifierGenerator procurementIdentifierGenerator;


    public PlantHireRequest createPlantHireRequest(PlantHireRequestDTO phrdto){
        PlantHireRequest phre = PlantHireRequest.of(PlantHireRequestID.of(phrdto.get_id()),phrdto.getPlantName(),phrdto.getPlant(),
                BusinessPeriod.of(phrdto.getRentalPeriod().getStartDate(), phrdto.getRentalPeriod().getEndDate()),
                phrdto.getCost(),phrdto.getComment(),phrdto.getStatus(), phrdto.getSiteId(), phrdto.getEmail());
        phrRepo.save(phre);
        return  phre;
    }

    public List<PlantHireRequest> findAllPHR(){
        return phrRepo.findAll();
    }

    public void upDatePHRStatus(Long id, PHRStatus phrStatus){
        PlantHireRequest pentry = phrRepo.findOne(PlantHireRequestID.of(id));
        pentry.decidePHRStatus(phrStatus);
        phrRepo.save(pentry);
    }

    public PlantHireRequest findOnePHR(Long id){
        PlantHireRequest pentry = phrRepo.findOne(PlantHireRequestID.of(id));
        return pentry;
    }

    public void updatePHR(PlantHireRequestDTO phrdto){
        phrRepo.save(PlantHireRequest.of(PlantHireRequestID.of(phrdto.get_id()), phrdto.getPlantName(),phrdto.getPlant(),
                BusinessPeriod.of(phrdto.getRentalPeriod().getStartDate(), phrdto.getRentalPeriod().getEndDate()),
                phrdto.getCost(),phrdto.getComment(),phrdto.getStatus(), phrdto.getSiteId(), phrdto.getEmail()));
    }

    public void createPurchaseOrder(PurchaseOrderDTO purchaseOrder){
        poRepo.save(PurchaseOrder.of(procurementIdentifierGenerator.hasNextPurchaseOrderID(), purchaseOrder.getPo()));
//                purchaseOrder.getPlant(),
//                BusinessPeriod.of(purchaseOrder.getRentalPeriod().getStartDate(),purchaseOrder.getRentalPeriod().getEndDate()),
//                purchaseOrder.getStatus()));
    }

    public  List<PurchaseOrder> findAllPOS(){
        return poRepo.findAll();
    }
    public PurchaseOrder getOneOrder(Long id){
        PurchaseOrder purchaseOrder = coRepo.findOne(PurchaseOrderID.of(id));
        return purchaseOrder;
    }

    public void updatePO(PurchaseOrderDTO poDTO){
        poRepo.save(PurchaseOrder.of(
                PurchaseOrderID.of(poDTO.get_id()), poDTO.getPo()
//                poDTO.getPlant(),
//                BusinessPeriod.of(poDTO.getRentalPeriod().getStartDate(), poDTO.getRentalPeriod().getEndDate()),
//                poDTO.getStatus()
        ));

    }

    public Long getUrlID(String url){
        return Long.valueOf(url.substring(url.lastIndexOf("/")+1, url.length()));
    }
}
