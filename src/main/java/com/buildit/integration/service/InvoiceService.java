package com.buildit.integration.service;

import com.buildit.hiring.application.domain.IDs.InvoiceID;
import com.buildit.hiring.application.domain.model.Invoice;
import com.buildit.hiring.application.domain.model.InvoiceStatus;
import com.buildit.hiring.application.domain.repository.InvoiceRepository;
import com.buildit.hiring.application.dto.InvoiceDTO;
import com.buildit.integration.service.idgenerator.ProcurementIdentifierGenerator;
import org.hibernate.id.IdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by omitobisam on 5.05.16.
 */
@Service
public class InvoiceService {
    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ProcurementIdentifierGenerator procureIDGen;

    public List<Invoice> getAllInvoice(){
        return invoiceRepository.findAll();
    }

    public Invoice findOneInvoice(Long loc){
        return  invoiceRepository.findOne(InvoiceID.of(loc));
    }


    public void createNewInvoice(InvoiceDTO invoiceDTO){
        Invoice inv = Invoice.of(procureIDGen.hasNextInvoiceID(),invoiceDTO.getPo(), invoiceDTO.getDate(), invoiceDTO.getTotal());
        inv.setInvoiceStatus(invoiceDTO.getInvoiceStatus());
        inv.setAcceptStatus(invoiceDTO.getIsAccepted());
        invoiceRepository.save(inv);
    }

}
