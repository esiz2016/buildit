package com.buildit.integration.service.idgenerator;

import com.buildit.common.infrastructure.HibernateBasedIdentifierGenerator;
import com.buildit.hiring.application.domain.IDs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by omitobisam on 30.03.16.
 */
@Service
public class ProcurementIdentifierGenerator {
    @Autowired
    HibernateBasedIdentifierGenerator hibernateGenerator;

    public PlantHireRequestID hasNextPlantHireRequestID() {
        return PlantHireRequestID.of(hibernateGenerator.getID("PlantHireRequestIDSequence"));
    }

    public InvoiceID hasNextInvoiceID() {
        return InvoiceID.of(hibernateGenerator.getID("InvoiceIDSequence"));
    }

    public PurchaseOrderID hasNextPurchaseOrderID (){ return  PurchaseOrderID.of(hibernateGenerator.getID("PurchaseOrderIDSequence"));}

}
