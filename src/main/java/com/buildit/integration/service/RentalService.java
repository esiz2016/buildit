package com.buildit.integration.service;

import com.buildit.common.infrastructure.exception.ResourceNotFoundException;
import com.buildit.hiring.application.dto.CoreOrderDTO;
import com.buildit.hiring.application.dto.PlantHireRequestDTO;
import com.buildit.hiring.application.dto.PurchaseOrderDTO;
import com.buildit.hiring.application.dto.PlantInventoryRepDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * Created by omitobisam on 30.03.16.
 */
@Service
public class RentalService {
    @Autowired
    RestTemplate restTemplate;


    @Value("${rentit.host:}")
    String host;
    @Value("${rentit.port:}")
    String port;

    @CrossOrigin
    public List<PlantInventoryRepDTO> findAvailablePlants(String plantName, LocalDate startDate, LocalDate endDate) {
        PlantInventoryRepDTO[] plants = restTemplate.getForObject(
                "http://"+ host + ":" + port + "/api/inventory/plants?name={name}&startDate={startDate}&endDate={endDate}",
                PlantInventoryRepDTO[].class, plantName, startDate, endDate);
        return Arrays.asList(plants);
    }


    //public PlantInventoryRepDTO findOnePlant(String plantName, LocalDate startDate, LocalDate endDate) {
    @CrossOrigin
    public PlantInventoryRepDTO findOnePlant(Long id){
        PlantInventoryRepDTO plant = restTemplate.getForObject(
                "http://" + host + ":" + port + "/api/inventory/plants/{id}", PlantInventoryRepDTO.class, id);
        return plant;

        /*try {
            ResponseEntity<PurchaseOrderDTO> result = restTemplate.postForEntity(
                    "http://" + host + ":" + port + "/api/sales/orders", order, PurchaseOrderDTO.class);
            return result.getBody();
        } catch (final HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.CONFLICT))
            ... // Do something to handle the exception
        }
        return null;*/
    }

    /*public CoreOrderDTO createPurchaseOrder(CoreOrderDTO order) throws PlantNotFoundException {
        ResponseEntity<CoreOrderDTO> result = restTemplate.postForEntity(
                "http://" + host + ":" + port + "/api/sales/orders", order, CoreOrderDTO.class);
        return result.getBody();
    }*/
    @CrossOrigin
    public CoreOrderDTO createPurchaseOrder(CoreOrderDTO coreOrderDTO) {
        try {
            ResponseEntity<CoreOrderDTO> result = restTemplate.postForEntity(
                    "http://" + host + ":" + port + "/api/sales/orders", coreOrderDTO, CoreOrderDTO.class);
            PurchaseOrderDTO poDTO = new PurchaseOrderDTO();
            poDTO.setPo(result.getBody().getId().getHref());
            return result.getBody();
        } catch (final HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.CONFLICT)){
                new ResourceNotFoundException();
            }
        }
        return null;
    }

    @CrossOrigin
    public CoreOrderDTO fetchPurchaseOrder(Long id)  {
        ResponseEntity<CoreOrderDTO> result = restTemplate.getForEntity(
                "http://" + host + ":" + port + "/api/sales/orders/"+id, CoreOrderDTO.class);
        return result.getBody();
    }

    @CrossOrigin
    public CoreOrderDTO makeOneExtension(Long id)  {
        ResponseEntity<CoreOrderDTO> result = restTemplate.getForEntity(
                "http://" + host + ":" + port + "/api/sales/orders/"+id, CoreOrderDTO.class);
        return result.getBody();
    }

}
