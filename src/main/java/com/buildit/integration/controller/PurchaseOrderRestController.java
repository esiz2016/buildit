package com.buildit.integration.controller;

import com.buildit.common.infrastructure.domain.BusinessPeriod;
import com.buildit.hiring.application.Assembler.PurchaseOrderAssembler;
import com.buildit.hiring.application.Assembler.PHRAssembler;
import com.buildit.hiring.application.domain.IDs.PurchaseOrderID;
import com.buildit.hiring.application.domain.model.POStatus;
import com.buildit.hiring.application.domain.model.PurchaseOrder;
import com.buildit.hiring.application.dto.*;
import com.buildit.integration.service.ProcurementService;
import com.buildit.integration.service.RentalService;
import com.buildit.integration.service.idgenerator.ProcurementIdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Created by omitobisam on 13.05.16.
 */

@RestController
@RequestMapping("/pos")
public class PurchaseOrderRestController {

    @Autowired
    ProcurementIdentifierGenerator procurementIdentifierGenerator;

    @Autowired
    RentalService rentalService;

    @Autowired
    ProcurementService procurementService;

    @Autowired
    PHRAssembler phrAssembler;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

//    @RequestMapping(method = GET, path = "")
//    public ResponseEntity<List<PurchaseOrderDTO>> getAllPOS() throws Exception{
//        List<PurchaseOrderDTO> poDTOS = purchaseOrderAssembler.toResources(procurementService.findAllPOS());
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.getAccessControlRequestHeaders();
//
//        return new ResponseEntity<>(poDTOS, headers, HttpStatus.OK);
//    }
    @CrossOrigin
    @RequestMapping(method = POST, path = "")
    public ResponseEntity<CoreOrderDTO> forwardPO(@RequestBody CoreOrderDTO coreOrderDTO){


        //coreOrderDTO.setPlant(plantHireRequestDTO.getId().getHref());
        CoreOrderDTO coreDTO = rentalService.createPurchaseOrder(coreOrderDTO);




//           System.out.println(coreDTO);

        PurchaseOrderDTO poDTO = new PurchaseOrderDTO();
//        poDTO.setPo(coreDTO.getId().getHref().toString());
        poDTO.setStatus(coreDTO.getStatus());
        poDTO.set_id(coreDTO.get_id());
        System.out.println(poDTO.getPo());
        System.out.println(poDTO.getId());

        procurementService.createPurchaseOrder(poDTO);
        procurementService.findOnePHR(poDTO.get_id());

//        List<PurchaseOrder> pos = procurementService.findAllPOS();
//        List<PurchaseOrderDTO> poDTOs = new ArrayList<>();
//        for (PurchaseOrder po: pos
//             ) {
//            poDTOs.add(purchaseOrderAssembler.toResource(po));
//        }
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(coreDTO, headers, HttpStatus.OK);
    }

    @RequestMapping(method = GET, path = "/{id}")
    public ResponseEntity<CoreOrderDTO> retrieveOneSubmittedPO(@PathVariable Long id){
        CoreOrderDTO coDTO = rentalService.fetchPurchaseOrder(id);
//        PurchaseOrderDTO poDTO = new PurchaseOrderDTO();
//        //poDTO.set_id(coDTO.get_id());
//
//        //poDTO.setPo(coDTO.getHref());
//        poDTO.setPo(coDTO.getId().getHref());
//        poDTO.setStatus(coDTO.getStatus());
//        System.out.println(coDTO.getStatus());
//        System.out.println(poDTO.getPo());
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<>(coDTO, headers, HttpStatus.OK);
    }

    /*@RequestMapping(method = GET, path = "/pos/{id}/internal") //spos is submitted pos
    public List<ResponseEntity<CoreOrderDTO>> viewOneSubmittedPO(@PathVariable Long id){
        List<PurchaseOrderDTO> poDTO =
        Long id = procurementService.getUrlID(id);
        CoreOrderDTO coDTO = rentalService.fetchPurchaseOrder(id);
        coDTOs.add(coDTO);
    }*/

    @RequestMapping(method = GET, path = "") //spos is submitted pos
    public ResponseEntity<List<CoreOrderDTO>> viewSubmittedPOS(){
        List<PurchaseOrder> pos = procurementService.findAllPOS();
        List<CoreOrderDTO> coDTOs = new ArrayList<>();
        for (PurchaseOrder po: pos
             ) {
            Long id = procurementService.getUrlID(po.getPo());
            CoreOrderDTO coDTO = rentalService.fetchPurchaseOrder(id);
            coDTOs.add(coDTO);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<>(coDTOs, headers, HttpStatus.OK);
    }

    @RequestMapping(method = POST, path = "/extension") //spos is submitted pos
    public ResponseEntity<CoreOrderDTO> requestPOExtension(@RequestBody CoreOrderDTO coreOrderDTO){
//        CoreOrderDTO coreOrderDTO = rentalService.fetchPurchaseOrder(coDTO.get_id());


        CoreOrderDTO coDTO = rentalService.createPurchaseOrder(coreOrderDTO);
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<>(coDTO, headers, HttpStatus.ACCEPTED);
    }

    //Invoice submission is by Email


    /*@RequestMapping(method = POST, path = "/{id}/extension")
    public ResponseEntity<CoreOrderDTO> extendPO(@PathVariable Long id){
        PurchaseOrderDTO poDTO = rentalService.fetchPurchaseOrder(id);
        CoreOrderDTO coreOrderDTO = rentalService.createPurchaseOrder(poDTO);
        procurementService.updatePO(poDTO);
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();
        return new ResponseEntity<>(coreOrderDTO, headers, HttpStatus.OK);
    }*/


   /* @RequestMapping(method = POST, path = "/reqPO")
    public ResponseEntity<PurchaseOrderDTO> requestPurchaseOrder() throws Exception {
        PlantInventoryRepDTO plandto = rentalService.findAvailablePlants("Truck", LocalDate.now(), LocalDate.now().plusDays(2)).get(1);
        PurchaseOrderDTO podto = new PurchaseOrderDTO();
        podto.setId();
        podto.setPoStatus();
        coreOrderdto.setPlant(hiredPlantDTO);
        coreOrderdto.setRentalPeriod(BusinessPeriod.of(LocalDate.now(), LocalDate.now().plusDays(2l)));
        coreOrderdto.set_id(procurementIdentifierGenerator.hasNextCoreOrderID().getId());

//        CoreOrderDTO coreOrderDTO = rentalService.createPurchaseOrder(coreOrderdto);
  *//*      procurementService.setCoreOrder(

                PurchaseOrder.of(PurchaseOrderID.of(coreOrderdto.get_id()), HiredPlantID.of(coreOrderdto.getPlant().get_id()),
                        coreOrderdto.getRentalPeriod(), coreOrderdto.getCost(), coreOrderdto.getPoStatus())
        );*//*

        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<PurchaseOrderDTO>(coreOrderdto, headers, HttpStatus.CREATED);
    }*/



  /*  @RequestMapping(method = POST, path = "/new")
    public ResponseEntity<CoreOrderDTO> createPO (@RequestBody PlantHireRequestDTO phrDTO) throws Exception{
        CoreOrderDTO coDTO = new CoreOrderDTO();
        PlantInventoryRepDTO plantDTO = new PlantInventoryRepDTO();
        String plantUrl = phrDTO.getPlant();
        Long plantID = Long.valueOf(plantUrl.substring(plantUrl.lastIndexOf("/")+1, plantUrl.length()));
        plantDTO.set_id(plantID);
        plantDTO.setPrice(phrDTO.getCost());
        //plantDTO.setName();
        coDTO.setPlant(plantDTO);
        coDTO.setRentalPeriod(phrDTO.getRentalPeriod());
        coDTO.setQuantity(1);
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();
        return  new ResponseEntity<>(coDTO, headers, HttpStatus.CREATED);

    }*/


    /*@RequestMapping(method = POST, path = "/{id}")
    public ResponseEntity<PurchaseOrder> approveOrder(@PathVariable Long id) throws Exception{
        PurchaseOrder purchaseOrder = procurementService.getOneOrder(PurchaseOrderID.of(id));
        purchaseOrder.approveCoreOrder();
        PurchaseOrderDTO coreOrderDTO =  purchaseOrderAssembler.toResource(purchaseOrder);
        rentalService.createPurchaseOrder(coreOrderDTO);
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<PurchaseOrder>(purchaseOrder, headers, HttpStatus.CREATED);
    }*/
}
