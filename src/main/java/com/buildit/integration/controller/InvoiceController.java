package com.buildit.integration.controller;

import com.buildit.hiring.application.Assembler.InvoiceAssembler;
import com.buildit.hiring.application.domain.model.Invoice;
import com.buildit.hiring.application.domain.model.InvoiceStatus;
import com.buildit.hiring.application.domain.repository.InvoiceRepository;
import com.buildit.hiring.application.dto.InvoiceDTO;
import com.buildit.integration.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omitobisam on 5.05.16.
 */
@Controller
@RequestMapping("/service")
public class InvoiceController {
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    InvoiceAssembler invoiceAssembler;
    @Autowired
    InvoiceRepository invoiceRepository;

    @RequestMapping("/show")
    public String showAllInvoices(Model model){
//        invoiceService.createNewInvoice();
        List<Invoice> invs = invoiceService.getAllInvoice();
        List<InvoiceDTO> invoiceDTOs = new ArrayList<>();
        for(Invoice invoice: invs){
            invoiceDTOs.add(invoiceAssembler.toResource(invoice));
        }
        model.addAttribute("Invoices", invoiceDTOs);
        return "show";
    }

    @RequestMapping(value="/show/{id}", method= RequestMethod.GET)
    public String processOneInvoice(Model model,InvoiceDTO invoiceDTO, @PathVariable Long id){
        Invoice inv = invoiceService.findOneInvoice(id);
        inv.setInvoiceStatus(InvoiceStatus.PAID);
        inv.setAcceptStatus(false);
        invoiceRepository.save(inv);
        invoiceDTO = invoiceAssembler.toResource(inv);
        model.addAttribute("Invoice", invoiceDTO);
        return "/result";
    }

}
