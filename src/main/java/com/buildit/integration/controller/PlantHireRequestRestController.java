package com.buildit.integration.controller;

import com.buildit.common.infrastructure.domain.BusinessPeriod;
import com.buildit.hiring.application.Assembler.PHRAssembler;
import com.buildit.hiring.application.Assembler.PurchaseOrderAssembler;
import com.buildit.hiring.application.domain.IDs.PlantHireRequestID;
import com.buildit.hiring.application.domain.IDs.PurchaseOrderID;
import com.buildit.hiring.application.domain.model.PHRStatus;
import com.buildit.hiring.application.domain.model.POStatus;
import com.buildit.hiring.application.domain.model.PlantHireRequest;
import com.buildit.hiring.application.domain.model.PurchaseOrder;
import com.buildit.hiring.application.dto.*;
import com.buildit.integration.service.ProcurementService;
import com.buildit.integration.service.RentalService;
import com.buildit.integration.service.idgenerator.ProcurementIdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;

import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Created by omitobisam on 30.03.16.
 */
@CrossOrigin
@RestController
@RequestMapping("/phrs")
public class PlantHireRequestRestController {

    @Autowired
    ProcurementIdentifierGenerator procurementIdentifierGenerator;

    @Autowired
    RentalService rentalService;

    @Autowired
    PurchaseOrderAssembler poAssembler;

    @Autowired
    ProcurementService procurementService;

    @Autowired
    PHRAssembler phrAssembler;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;


    /*@RequestMapping(method = GET, path = "/plants")
    public List<PlantInventoryRepDTO> queryPlantCatalog(
            @RequestParam(name="name", required = false) String plantName,
            @RequestParam(name="startDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name="endDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate endDate) {
        System.out.println("Hello I got here");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startDate_ = LocalDate.parse(startDate, formatter);
        LocalDate endDate_ = LocalDate.parse(endDate, formatter);
        return rentalService.findAvailablePlants(plantName, startDate, endDate);
    }*/


    @RequestMapping(method = GET, path = "/plants")
    public ResponseEntity<List<PlantInventoryRepDTO>> getAllPlants(
            @RequestParam(name="name", required = false) String plantName,
            @RequestParam(name="startDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name="endDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate endDate) throws IOException {
        List<PlantInventoryRepDTO> plantRepDTOs = rentalService.findAvailablePlants(plantName,startDate,endDate);

        HttpHeaders headers = new HttpHeaders();
        //headers.getAccessControlRequestHeaders();
//        headers.setAccessControlRequestMethod(HttpMethod.GET);


        return new ResponseEntity<>(plantRepDTOs, headers, HttpStatus.OK);
    }

    @RequestMapping(method = GET, path = "/plants/{id}")
    public ResponseEntity<PlantInventoryRepDTO> GetOnePlant(@PathVariable Long id){
        PlantInventoryRepDTO plantRepDTO = rentalService.findOnePlant(id);
        //PlantInventoryRepDTO plantRepDTO = rentalService.findOnePlant(id);
//        plantRepDTO.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(3l)));
        HttpHeaders headers = new HttpHeaders();
        headers.setAccessControlAllowOrigin("*");

        return new ResponseEntity<>(plantRepDTO, headers, HttpStatus.CREATED);
    }

/*    @RequestMapping(method = GET, path = "/plants/one/retrieved")
    public HiredPlantDTO saveOnePlant(
            @RequestParam(name="name", required = false) String plantName,
            @RequestParam(name="startDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name="endDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate endDate) {
        PlantInventoryRepDTO plantRepDTO = rentalService.findOnePlant(plantName, startDate, endDate);
        HiredPlantDTO hpdto = new HiredPlantDTO();
        hpdto.setName(plantRepDTO.getName());
        hpdto.setPlantURL(plantRepDTO.getId().getHref());
//        System.out.println(plantRepDTO.getId().getHref());
        procurementService.setPlantHireRequest(hpdto, PHRStatus.PENDING);
        return hpdto;
    }*/

    @RequestMapping(method = POST, path = "")
    public ResponseEntity<PlantHireRequestDTO> createNewPHR(@RequestBody VIrtualPlantDTO plantRepDTO) throws Exception {
        //@RequestBody PlantInventoryRepDTO plantRepDTO
        //PlantInventoryRepDTO plantRepDTO = GetOnePlant(LocalDate.now().toString(), LocalDate.now(), LocalDate.now()); //Request body should be used instead
        PlantHireRequestDTO phrDTO = new PlantHireRequestDTO();
        phrDTO.set_id(procurementIdentifierGenerator.hasNextPlantHireRequestID().getId());
        phrDTO.setPlantName(plantRepDTO.getName());
        phrDTO.setPlant(plantRepDTO.getId().getHref());
        phrDTO.setCost(plantRepDTO.getPrice());
        phrDTO.setRentalPeriod(plantRepDTO.getRentalPeriod());
        phrDTO.setStatus(PHRStatus.PENDING_CONFIRMATION);
        /*phrDTO.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now()));
        PurchaseOrder po = PurchaseOrder.of(PurchaseOrderID.of(Link.valueOf("{'link':'http://localhost:8080/pos/1'}")), POStatus.ACCEPTED );*/
       // procurementService.createPurchaseOrder(poAssembler.toResource(po));
        /*PurchaseOrderDTO poDTO = new PurchaseOrderDTO();
        poDTO.set_id(procurementIdentifierGenerator.hasNextPurchaseOrderID().getHref());
        poDTO.setPoStatus(POStatus.PENDING);
        procurementService.createPurchaseOrder(poDTO);*/

        //phrDTO.setPurchaseOrder(poDTO);
        phrDTO.setComment("Not yet");
        phrDTO.setSiteId("");
        phrDTO.setEmail("rentit555@gmail.com");


        procurementService.createPlantHireRequest(phrDTO);

        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<>(phrDTO, headers, HttpStatus.CREATED);
    }


    @RequestMapping(method = GET, path = "/{id}")
    public ResponseEntity<PlantHireRequestDTO> getOnePHR(@PathVariable Long id){ //we could search by phr whole info too

        PlantHireRequestDTO phrDTO = phrAssembler.toResource(procurementService.findOnePHR(id));
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();
        return  new ResponseEntity<>(phrDTO, headers, HttpStatus.OK);
    }

    @RequestMapping(method = GET, path = "")
    public List<PlantHireRequestDTO> getAllPHR(){

        List<PlantHireRequest> phrs = procurementService.findAllPHR();

        List<PlantHireRequestDTO> phrDTOs = new ArrayList<>();
        for (PlantHireRequest phr: phrs
             ) {
            phrDTOs.add(phrAssembler.toResource(phr));
        }

        return phrDTOs;
    }


    //check status of plan hire request
    @RequestMapping(method = GET, path = "/{id}/status")
    public PHRStatus  getStatusOfPHR(@PathVariable Long id){
        PlantHireRequestDTO phrDTO = phrAssembler.toResource(procurementService.findOnePHR(id));
        return phrDTO.getStatus();
    }

    // cancel PHR, only before due date
    @RequestMapping(method = POST, path = "/{id}/cancel")
    public String cancelPHR(@PathVariable Long id) {
        PlantHireRequest phr = procurementService.findOnePHR(id);
        // now we look if the date in PHR is after today, to successful canceling
        if (phrAssembler.toResource(phr).getRentalPeriod().getStartDate().isAfter(LocalDate.now())) {

            //if (phrAssembler.toResource(phr).getPurchaseOrder().getPoStatus() == POStatus.ACCEPTED) {
               // return "Canceling is not possible due to the PO status";

            procurementService.upDatePHRStatus(phr.getId().getId(), PHRStatus.CANCELED);
            return "PHR canceled sucessfully";
        }
        //procurementService.upDatePHRStatus(phr.getId().getId(), PHRStatus.CANCELED);
        //PlantHireRequestDTO phrDTO = phrAssembler.toResource(phr);
        //HttpHeaders headers = new HttpHeaders();
        //headers.getAccessControlRequestHeaders();
        else return "Canceling is not possible, too late";
    }


    @RequestMapping(method = GET, path = "/{id}/editable")
    public ResponseEntity<PlantHireRequestDTO> editablePHR(@PathVariable Long id) throws Exception{

        PlantHireRequestDTO plantHireRequestDTO =phrAssembler.toResource(procurementService.findOnePHR(id));
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();
        if (!plantHireRequestDTO.getStatus().equals(PHRStatus.APPROVED)){
            return new ResponseEntity<>(plantHireRequestDTO, headers, HttpStatus.OK);
        }else {
            return null;
        }
    }


    @RequestMapping(method = PUT, path = "/{id}/upd")
    public ResponseEntity<PlantHireRequestDTO> updatePHR(@RequestBody PlantHireRequestDTO phrDTO){

        PHRStatus phrStatus = procurementService.findOnePHR(phrDTO.get_id()).getStatus();
        if(phrStatus.equals(PHRStatus.APPROVED)){
                return null;
        } else {
            procurementService.updatePHR(phrDTO);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();
        return  new ResponseEntity<>(phrDTO, headers, HttpStatus.OK);
    }


//    public PurchaseOrderDTO preApprovePHR(CoreOrderDTO coDTO){
//        PurchaseOrderDTO poDTO = rentalService.createPurchaseOrder(coDTO);
////        System.out.println(poDTO.get_id());
//        System.out.println(poDTO.getPo());
//        System.out.println(poDTO.getId());
//
//
//        procurementService.createPurchaseOrder(poDTO);
//        List<PurchaseOrder> pos = procurementService.findAllPOS();
//        List<PurchaseOrderDTO> poDTOs = new ArrayList<>();
//        for (PurchaseOrder po: pos
//                ) {
//            poDTOs.add(purchaseOrderAssembler.toResource(po));
//        }
//        return poDTO;
//    }

    @RequestMapping(method = GET, path = "/{id}/approval")
    public ResponseEntity<CoreOrderDTO> approvePHR(@PathVariable Long id){

        PlantHireRequest phr = procurementService.findOnePHR(id);
        procurementService.upDatePHRStatus(phr.getId().getId(), PHRStatus.APPROVED);

        CoreOrderDTO coDTO = new CoreOrderDTO();
        PlantHireRequestDTO phrDTO = phrAssembler.toResource(phr);

            String plantUrl = phrDTO.getPlant();
            Long plantID = procurementService.getUrlID(plantUrl);  //converts the plantURl to the id of the plant in long
            //----------------------------------------------------//

            PlantInventoryRepDTO plantDTO = rentalService.findOnePlant(plantID);
            coDTO.setPlant(plantDTO);
            coDTO.setRentalPeriod(phrDTO.getRentalPeriod());
            coDTO.setEmail("buildit555@gmail.com");

            //----Now create the po by
            /*PurchaseOrderDTO poDTO = rentalService.createPurchaseOrder(coDTO); //finds creates the po with plant rep and url
//        System.out.println(poDTO.get_id());
            System.out.println(poDTO.getPo());
            System.out.println(poDTO.getId());*/


           // procurementService.createPurchaseOrder(poDTO);  //the PO added to the system

            //coDTO.setStatus(phrDTO.getStatus());
//            List<PurchaseOrder> pos = procurementService.findAllPOS(); //lists all po in Buildit's system
//            List<PurchaseOrderDTO> poDTOs = new ArrayList<>();
//            for (PurchaseOrder po: pos   //for each internal registered po
//                    ) {
//                poDTOs.add(purchaseOrderAssembler.toResource(po)); //converts to po reps to check
//            }



        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<>(coDTO, headers, HttpStatus.ACCEPTED);
    }

    @RequestMapping(method = GET, path = "/{id}/rejection")
    public ResponseEntity<PlantHireRequestDTO> rejectPHR(@PathVariable Long id){

        PlantHireRequest phr = procurementService.findOnePHR(id);
        procurementService.upDatePHRStatus(phr.getId().getId(), PHRStatus.REJECTED);
        PlantHireRequestDTO phrDTO = phrAssembler.toResource(phr);
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<>(phrDTO, headers, HttpStatus.ACCEPTED);
    }


    /*@RequestMapping(method = PUT, path = "/plants/one/approval")
    public PlantHireRequestDTO approvePurchaseOrder (){
       System.out.println("Hello I got here");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startDate_ = LocalDate.parse(startDate, formatter);
        LocalDate endDate_ = LocalDate.parse(endDate, formatter);
        PlantHireRequestEntry pentry = procurementService.upDatePHRStatus(1l);
        PlantHireRequestDTO plantHireRequestEntryDTO = phrAssembler.toResource(pentry);
        return  plantHireRequestEntryDTO;
    }*/


}
