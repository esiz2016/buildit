package com.buildit.integration.controller;

import com.buildit.hiring.application.Assembler.InvoiceAssembler;
import com.buildit.hiring.application.domain.IDs.InvoiceID;
import com.buildit.hiring.application.domain.model.Invoice;
import com.buildit.hiring.application.domain.model.InvoiceStatus;
import com.buildit.hiring.application.domain.repository.InvoiceRepository;
import com.buildit.hiring.application.dto.InvoiceDTO;
import com.buildit.integration.service.InvoiceService;
import com.buildit.integration.service.ProcurementService;
import com.buildit.integration.service.idgenerator.ProcurementIdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Created by omitobisam on 5.05.16.
 */
@CrossOrigin
@RestController
@RequestMapping("/invoices")
public class InvoiceRestController {
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    InvoiceAssembler invoiceAssembler;
    @Autowired
    InvoiceRepository invoiceRepository;



    @RequestMapping(method = PUT, path = "")
    public InvoiceDTO makePayment(@RequestBody InvoiceDTO invoiceDTO){
        boolean acceptStatus = invoiceDTO.getIsAccepted();
        if(acceptStatus==true){
            invoiceDTO.setInvoiceStatus(InvoiceStatus.PAID);
            invoiceService.createNewInvoice(invoiceDTO);
        }
        return invoiceDTO;
    }


    @RequestMapping(method = GET, path = "")
    public ResponseEntity<List<InvoiceDTO>> showAllInvoice(){
/*        InvoiceDTO invoiceDTO = invoiceAssembler.toResource(inv.of(
                new ProcurementIdentifierGenerator().hasNextInvoiceID(),
                LocalDate.now(),
                new BigDecimal(13.5)
        ));
        invoiceRepository.save(inv);
        */

        List<InvoiceDTO> invDTOs= invoiceAssembler.toResources(invoiceService.getAllInvoice());
        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<>(invDTOs, headers, HttpStatus.OK);
    }


    @RequestMapping(method = GET, path = "/{id}")
    public ResponseEntity<InvoiceDTO> getOneInvoice(@PathVariable Long id){
        InvoiceDTO invoiceDTO =  invoiceAssembler.toResource(invoiceService.findOneInvoice(id));

        HttpHeaders headers = new HttpHeaders();
        headers.getAccessControlRequestHeaders();

        return new ResponseEntity<>(invoiceDTO, headers, HttpStatus.ACCEPTED);
    }




}
