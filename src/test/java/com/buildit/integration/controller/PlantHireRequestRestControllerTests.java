//package com.buildit.integration.controller;
//
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.is;
//import static org.hamcrest.core.IsNull.notNullValue;
//import static org.junit.Assert.assertEquals;
//import com.buildit.BuildItApplication;
//import com.buildit.common.infrastructure.domain.BusinessPeriod;
//import com.buildit.common.infrastructure.exception.PlantNotFoundException;
//import com.buildit.hiring.application.Assembler.PHRAssembler;
//import com.buildit.hiring.application.domain.model.PHRStatus;
//import com.buildit.hiring.application.dto.*;
//import com.buildit.integration.service.ProcurementService;
//import com.buildit.integration.service.RentalService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.context.annotation.Bean;
//import org.springframework.hateoas.Link;
//import org.springframework.http.HttpRequest;
//import org.springframework.http.MediaType;
//import org.springframework.http.client.ClientHttpRequestExecution;
//import org.springframework.http.client.ClientHttpRequestInterceptor;
//import org.springframework.http.client.ClientHttpResponse;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.TestContext;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.web.client.RestClientException;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.context.WebApplicationContext;
//
//import java.io.IOException;
//import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
//import static org.mockito.Matchers.isNotNull;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
///**
// * Created by omitobisam on 30.03.16.
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = {TestContext.class, BuildItApplication.class})
//@WebAppConfiguration
//@ActiveProfiles("test")
//public class PlantHireRequestRestControllerTests {
//    @Autowired
//    private WebApplicationContext wac;
//    private MockMvc mockMvc;
//
//    @Autowired @Qualifier("_halObjectMapper")
//    ObjectMapper mapper;
//
//    @Autowired
//    RentalService rentalService;
//
//    @Autowired
//    ProcurementService procurementService;
//
//    @Autowired
//    PlantHireRequestRestController plantHireRequestController;
//
//    @Autowired
//    PHRAssembler phrAssembler;
//
//    @Autowired
//    RestTemplate restTemplate;
//
//    List<ClientHttpRequestInterceptor> savedInterceptors = null;
//    @Before
//    public void setup() {
//        savedInterceptors = restTemplate.getInterceptors();
//    }
//    @After
//    public void tearoff() {
//        restTemplate.setInterceptors(savedInterceptors);
//    }
//
//    @Bean
//    public RestTemplate restTemplate() {
//        RestTemplate _restTemplate = new RestTemplate();
//        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
//        messageConverters.add(new MappingJackson2HttpMessageConverter());
//        _restTemplate.setMessageConverters(messageConverters);
//        return _restTemplate;
//    }
//
//
//    @Test
//    public void testCreatePurchaseOrder() throws PlantNotFoundException {
//        PlantInventoryRepDTO plant = new PlantInventoryRepDTO();
//        plant.add(new Link("http://localhost:3000/api/inventory/plants/13"));
//
//        PurchaseOrderDTO order = new PurchaseOrderDTO();
//        HiredPlantDTO hiredPlantDTO = new HiredPlantDTO();
//        hiredPlantDTO.setName(plant.getName());
//        hiredPlantDTO.setPlantURL(plant.getId().getHref());
//        order.setPlant(hiredPlantDTO);
//        order.setRentalPeriod(BusinessPeriod.of(LocalDate.now(), LocalDate.now().plusDays(4)));
//
//        rentalService.createPurchaseOrder(order);
//    }
//
//
//    @Test
//    public void testRejection() throws RestClientException, IOException {
//        ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
//            public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
//
//                return execution.execute(request, body);
//            }
//        };
//        restTemplate.setInterceptors(Collections.singletonList(interceptor));
//
//        Long poid = 100l;
//        PurchaseOrderDTO order = new PurchaseOrderDTO();
//        order.add(new Link("http://localhost:3000/api/sales/orders/"+poid));
//
//        rentalService.fetchPurchaseOrder(order, poid);
//    }
//
//
//
//    @Test(expected = NullPointerException.class)
//    public void testCreatePHRAndRejectPO() throws Exception {
//
//        ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
//            public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
//                request.getHeaders().add("prefer", "409");
//                return execution.execute(request, body);
//            }
//        };
//        restTemplate.setInterceptors(Collections.singletonList(interceptor));
//
//        List<PlantInventoryRepDTO> theplants = rentalService.findAvailablePlants("Truck", LocalDate.of(2016,11,12),LocalDate.of(2016,11,14));
//
//        PlantHireRequestDTO phrDTO = new PlantHireRequestDTO();
//
//        PurchaseOrderDTO order = new PurchaseOrderDTO();
//        HiredPlantDTO hiredPlantDTO = new HiredPlantDTO();
//        hiredPlantDTO.setName(theplants.get(0).getName());
//        hiredPlantDTO.setPlantURL(theplants.get(0).getId().getHref());
//        order.setPlant(hiredPlantDTO);
//        order.setRentalPeriod (BusinessPeriod.of(LocalDate.now(), LocalDate.now().plusDays(2l)));
//        order.set_id(1l);
//        order.add(new Link("http://localhost:3000/api/sales/orders", "self"));
//
//        phrDTO.setPlant(theplants.get(0));
//        phrDTO.setPhrStatus(PHRStatus.APPROVED);
//        phrDTO.set_id(order.get_id());
//
//        MvcResult result = mockMvc.perform(post("/phrs/")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(mapper.writeValueAsString(phrDTO))
//        )
//                .andExpect(status().isCreated())
//                .andReturn();
//
//        PlantHireRequestDTO phrDTOResponse = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);
//        assertThat(phrDTOResponse.getLink("approve"), is(notNullValue()));
//
//        Link approve = phrDTOResponse.getLink("approve");
//
//        result = mockMvc.perform(post(approve.getHref()))
//                .andExpect(status().isAccepted())
//                .andReturn();
//
//        phrDTOResponse = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);
//        assertThat(phrDTOResponse.getLink("self"), is(notNullValue()));
//        assertThat(phrDTOResponse.getLink("submitPurchaseOrder"), is(notNullValue()));
//        assertThat(phrDTOResponse.getLinks().size(), is(1));
//
//        assertEquals(order, phrDTOResponse.get_id());
//    }
//
///*
//    @Test
//    public void testGetAllPlants() throws Exception {
//        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
//        List<PlantInventoryRepDTO> list =
//                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryRepDTO>>() { });
//        LocalDate startDate = LocalDate.now();
//        LocalDate endDate = startDate.plusDays(2);
//        when(rentalService.findAvailablePlants("Truck", startDate, endDate)).thenReturn(list);
//
//        PlantInventoryRepDTO plant1 = list.get(1);
//        System.out.println(plant1);
//        MvcResult result = mockMvc.perform(
//                get("/phrs/plants?name=Truck&startDate="+startDate+"&endDate="+endDate))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        // Add additional test expectations
//    }*/
//
//   /* @Test
//    public void testGetOnePlant() throws Exception {
//        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
//        List<PlantInventoryRepDTO> list =
//                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryRepDTO>>() { });
//        LocalDate startDate = LocalDate.now();
//        LocalDate endDate = startDate.plusDays(2);
//        when(rentalService.findOnePlant("Truck", startDate, endDate)).thenReturn(list.get(1));
//        MvcResult result = mockMvc.perform(
//                get("/phrs/plants/one?name=Truck&startDate="+startDate+"&endDate="+endDate))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        // Add additional test expectations
//    }
//
//    @Test
//    public void testAcceptPlantHireRequest() throws Exception {
//        Resource responseBody = new ClassPathResource("phr.json", this.getClass());
//        List<PlantHireRequestDTO> list =
//                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantHireRequestDTO>>() { });
//
//        when( phrAssembler.toResource(procurementService.upDatePHRStatus(1l))).thenReturn(list.get(1));
//        MvcResult result = mockMvc.perform(
//                get("/phrs/plants/one/approval"))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        // Add additional test expectations
//    }*/
//
//}
