FORMAT: 1A
HOST: http://localhost:3000

# ERentIt
Excerpt of RentIt's API

# Group Inventory
Notes related resources of the **Plants API**

## Plant Catalog [/api/inventory/plants{?name,startDate,endDate}]
### Retrieve Plants [GET]
+ Parameters
    + name (optional,string) ... Name of the plant
    + startDate (optional,date) ... Starting date for rental
    + endDate (optional,date) ... End date for rental

+ Response 200 (application/json)

        [
            {"_id":13,"name":"D-Truck","description":"15 Tonne Articulating Dump Truck","price":250.00,"_links":{"self":{"href":"http://localhost:3000/api/inventory/plants/13"}}},
            {"_id":14,"name":"D-Truck","description":"30 Tonne Articulating Dump Truck","price":300.00,"_links":{"self":{"href":"http://localhost:3000/api/inventory/plants/14"}}}
        ]

## View Plant [/api/inventory/plants/{id}]
### Retrieve a single Plant [GET]
+ Parameters
    + id (integer) ... ID of the requested plant
    
+ Response 200 (application/json)

            {"_id":13,"name":"D-Truck","description":"15 Tonne Articulating Dump Truck","price":250.00,"_links":{"self":{"href":"http://localhost:3000/api/inventory/plants/13"}}}

# Group Orders
Purchase orders, and other management actions

## Purchase Order - Container [/api/sales/orders]
### Create Purchase Order [POST]
+ Request (application/json)

        {
            "plant":{
                "_links":{"self":{"href":"http://localhost:3000/api/inventory/plants/13"}}
            },
            "rentalPeriod": {"startDate":"2016-11-12", "endDate":"2016-11-14"}
        }

+ Response 201 (application/json)

    + Headers

            Location: http://localhost:3000/api/sales/orders/100

    + Body

            {
                "plant": {
                    "name": "D-Truck",
                    "description": "15 Tonne Articulating Dump Truck",
                    "price": 250,
                    "_links": {
                        "self": {"href": "http://localhost:3000/api/inventory/plants/13"}
                    }
                },
                "rentalPeriod": {"startDate": "2016-11-12", "endDate": "2016-11-14"},
                "status": "OPEN",
                "total": 750,
                "_links": {
                    "self": {"href": "http://localhost:3000/api/sales/orders/100"}
                },
                "_xlinks": [
                    {
                        "_rel": "extend",
                        "href": "http://localhost:3000/api/sales/orders/100/extensions",
                        "method": "POST"
                    }
                ]
            }

## View  [/api/sales/orders/{id}]
### Extend Purchase Order [GET]
+ Parameters
    + id (integer) ... ID of the order

+ Response 200 (application/json)

    + Body

            {
                "plant": {
                    "name": "D-Truck",
                    "description": "15 Tonne Articulating Dump Truck",
                    "price": 250,
                    "_links": {
                        "self": {"href": "http://localhost:3000/api/inventory/plants/13"}
                    }
                },
                "rentalPeriod": {"startDate": "2016-11-12", "endDate": "2016-11-14"},
                "status": "OPEN",
                "total": 750,
                "_links": {
                    "self": {"href": "http://localhost:3000/api/sales/orders/100"}
                }
            }

## Extend Order [/api/sales/orders/{id}/extension]
### Extend Purchase Order [POST]
+ Request (application/json)

        {
            "plant":{
                "_links":{"self":{"href":"http://localhost:3000/api/inventory/plants/13"}}
            },
            "rentalPeriod": {"startDate":"2016-11-15", "endDate":"2016-11-18"}
        }

+ Response 201 (application/json)

    + Headers

            Location: http://localhost:3000/api/sales/orders/100

    + Body

            {
                "plant": {
                    "name": "D-Truck",
                    "description": "15 Tonne Articulating Dump Truck",
                    "price": 250,
                    "_links": {
                        "self": {"href": "http://localhost:3000/api/inventory/plants/13"}
                    }
                },
                "rentalPeriod": {"startDate": "2016-11-12", "endDate": "2016-11-14"},
                "status": "OPEN",
                "total": 750,
                "_links": {
                    "self": {"href": "http://localhost:3000/api/sales/orders/100"}
                },
                "_xlinks": [
                    {
                        "_rel": "extend",
                        "href": "http://localhost:3000/api/sales/orders/100/extensions",
                        "method": "POST"
                    }
                ]
            }
